#ifndef PERSON_H
#define PERSON_H
#include <opencv2/imgproc.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/video/background_segm.hpp>
#include <opencv2/tracking.hpp>
#include <opencv2/objdetect.hpp>

class Person {
  private:
    cv::Rect detectionROI;
    cv::Rect trackingROI;
    double distanceTravelled {0};
    bool detected { false };
    bool useDetection { false };
    int groupedFrames {0};
  public:
    int missedDetections {0};

    Person( const cv::Rect &bounds );
    Person() {}
    bool isInBounds( const cv::Rect &contourBound );
    double getDistanceFromTracking( const cv::Rect &bounds );
    void draw( cv::Mat &inputFrame );

    void detectionMiss() {
        ++missedDetections;
    }

    bool isLost() {//TODO stationary bounds rethink mb?
        return ( missedDetections > 15 && distanceTravelled < ( trackingROI.width / 2. ) ) || missedDetections > 50;
    }

    void detection( cv::Rect &bounds ) {
        detectionROI = bounds;
        detected = true;
        missedDetections = 0;
        distanceTravelled = 0;
    }

    bool isDetected() {
        return detected;
    }

    bool shouldUseDetection() {
        auto ret = useDetection;
        useDetection = false;
        return ret;
    }

    void grouped() {
        ++groupedFrames;
    }

    void notGrouped() {
        groupedFrames = 0;
    }

    bool isOverlapped() {
        return groupedFrames > 10;
    }

    void resetDetection() {
        detected = false;
    }

    void checkResetTracking() {
        useDetection = true;
    }

    const cv::Rect &getDetectionROI() {
        return detectionROI;
    }

    const cv::Rect &getROI() {
        return trackingROI;
    }

    inline void setROI( cv::Rect &newROI ) {
        distanceTravelled += getDistanceFromTracking( newROI );
        trackingROI = newROI;
    }

};

#endif // PERSON_H
