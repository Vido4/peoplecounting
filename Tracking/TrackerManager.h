#ifndef TRACKER_H
#define TRACKER_H
#include <vector>
#include <iostream>
#include <QRect>
#include <QHash>
#include <QVector>
#include "KCF/kcftracker.hpp"
#include "Person.h"

class TrackerManager {
  private:
    double areaThreshold;
    bool init = false;
    cv::Ptr<cv::BackgroundSubtractor> bgSubstractor;
    cv::Mat closingKernel;
    cv::Mat openingKernel;
    cv::Rect2d openROI;
    cv::Rect regionOfInterest;
    cv::Ptr<KCFTracker> kcfTracker;
    QHash<qint32, QPair<cv::Ptr<KCFTracker>, Person>> kcfTrackers;

    cv::Ptr<cv::HOGDescriptor> hog;

    static constexpr int CELL_SIZE = 6;
    bool ENABLED_HOG_RECT = false;
    bool ENABLED_HOG_DETECTED_RECT = false;
    bool RAW_PIXELS = false;
    bool DEBUG_MODE = false;

    void performSingleTracking( cv::Mat &inputFrame );
    void performMultiTracking( cv::Mat &inputFrame, int currentFrame );
    void updateCurrentTracking( cv::Mat &inputFrame );
    void updateCurrentKCFTracking( cv::Mat &inputFrame );
    void initNewTracking( const cv::Mat &inputFrame, const cv::Rect &bounds );
    void initNewKCFTracking( const cv::Mat &inputFrame, const cv::Rect &bounds );
    bool hasMinimumPersonArea( const cv::Rect &bounds );
    bool adjustToPreferredArea( cv::Rect &bounds );
    cv::Mat extractBlobs( const cv::Mat &inputFrame );
    void findAndValidatePersons( cv::Mat &resized, cv::Mat &inputFrame, const cv::Mat &foregroundFrame );
    void addNewPerson( cv::Mat &inputFrame, cv::Rect &foundPerson );
    void addNewKCFPerson( cv::Mat &inputFrame, cv::Rect &foundPerson );
    cv::Mat getHogAdjustedRect( cv::Mat &inputFrame, cv::Rect rect, cv::Point contourCenter );
    void adjustHogRect( cv::Mat &inputFrame, cv::Rect &rect );
    void removeOverlap();
    bool reLabelTrackers( cv::Rect &foundPerson );
    void adjustHogResponse( cv::Rect &foundRect );
    void processBlob( cv::Mat &inputFrame, const std::vector<cv::Point> &contour );
    void validateTrackers( cv::Mat &inputFrame );

    //    void updateText(cv::Mat &inputFrame){}
    //    void updateLines(cv::Mat &inputFrame){}
  public:
    TrackerManager();
    bool performTracking( cv::Mat &inputFrame, int currentFrame );
    void resetAlgorithmState();
    void setROI( const QRect &minumumThreshold );
    double getMinimumThreshold();
    std::pair<double, double> getMaximumThreshold();
};

#endif // TRACKER_H
