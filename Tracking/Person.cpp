#include "Person.h"

Person::Person( const cv::Rect &bounds )
    : trackingROI( bounds ), detectionROI( bounds ) {
}

bool Person::isInBounds( const cv::Rect &contourBound ) {
    static const double THRESHOLD = 1.85;

    bool topLeftNeighbour = std::abs( contourBound.tl().x - trackingROI.tl().x ) < contourBound.width / THRESHOLD
                            && std::abs( contourBound.tl().y - trackingROI.tl().y ) < contourBound.height / THRESHOLD;

    bool topRightNeighbour = std::abs( ( contourBound.tl().x + contourBound.width ) - ( trackingROI.tl().x + trackingROI.width ) )
                             < contourBound.width / THRESHOLD && std::abs( contourBound.tl().y - trackingROI.tl().y ) < contourBound.height / THRESHOLD;

    bool bottomLeftNeighbour = std::abs( ( contourBound.br().x - contourBound.width ) - ( trackingROI.br().x - trackingROI.width ) )
                               < contourBound.width / THRESHOLD && std::abs( contourBound.br().y - trackingROI.br().y ) < contourBound.height / THRESHOLD;

    bool bottomRightNeighbour = std::abs( contourBound.br().x - trackingROI.br().x ) < contourBound.width / THRESHOLD
                                && std::abs( contourBound.br().y - trackingROI.br().y ) < contourBound.height / THRESHOLD;

    return topLeftNeighbour || topRightNeighbour || bottomLeftNeighbour || bottomRightNeighbour;
}

double Person::getDistanceFromTracking( const cv::Rect &bounds ) {
    auto xDist = ( trackingROI.x + trackingROI.width / 2. ) - ( bounds.x + bounds.width / 2. );
    auto yDist = ( trackingROI.y + trackingROI.height / 2. ) - ( bounds.y + bounds.height / 2. );
    return sqrt( xDist * xDist + yDist * yDist );
}

void Person::draw( cv::Mat &inputFrame ) {
    cv::rectangle( inputFrame, trackingROI, cv::Scalar( 0, 0, 255 ), 1, 1 );
}
