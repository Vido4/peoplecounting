#include "TrackerManager.h"
#include <QElapsedTimer>
#include <QDebug>
#include <cmath>
#define DEBUG

TrackerManager::TrackerManager() {
    bgSubstractor = cv::createBackgroundSubtractorMOG2();
    closingKernel = cv::getStructuringElement( cv::MORPH_RECT, cv::Size( 11, 11 ) );
    openingKernel = cv::getStructuringElement( cv::MORPH_RECT, cv::Size( 3, 3 ) );
    hog = new cv::HOGDescriptor();
    hog->setSVMDetector( cv::HOGDescriptor::getDefaultPeopleDetector() );
}

cv::Mat TrackerManager::extractBlobs( const cv::Mat &inputFrame ) {
    cv::Mat outputFrame;
    cv::Mat noShadowOutputFrame;

    bgSubstractor->apply( inputFrame, outputFrame );
    cv::threshold( outputFrame, outputFrame, 200, 255, cv::THRESH_BINARY );
    cv::morphologyEx( outputFrame, outputFrame, cv::MORPH_OPEN, openingKernel );
    cv::morphologyEx( outputFrame, outputFrame, cv::MORPH_CLOSE, closingKernel );

    if ( DEBUG_MODE ) {
        imshow( "BackgroundSub", outputFrame );
    }

    return outputFrame;
}

bool TrackerManager::performTracking( cv::Mat &inputFrame, int currentFrame ) {
    if ( openROI.empty() ) {
        performMultiTracking( inputFrame, currentFrame );
    } else {
        performSingleTracking( inputFrame );
    }

    return true;
}

void TrackerManager::performSingleTracking( cv::Mat &inputFrame ) {
    if ( init ) {
        kcfTracker = new KCFTracker();
        kcfTracker->init( regionOfInterest, inputFrame );
        init = false;
    }

    static qint64 time = 0;
    static int counter = 0;
    QElapsedTimer timer;
    timer.start();

    regionOfInterest = kcfTracker->update( inputFrame );

    time += timer.elapsed();
    counter++;

    if ( ( counter % 25 ) == 0 ) {
        std::cout << "Operation took" << time / 25. << "milliseconds" << std::endl;
        time = 0;
    }

    cv::rectangle( inputFrame, regionOfInterest, cv::Scalar( 255, 0, 0 ), 1, 1 );
}

void TrackerManager::performMultiTracking( cv::Mat &inputFrame, int currentFrame ) {
    cv::Mat foregroundFrame = extractBlobs( inputFrame );
    static int counter = 0;
    static qint64 time = 0;
    static qint64 trackers = 0;
    QElapsedTimer timer;
    timer.start();

    updateCurrentKCFTracking( inputFrame );

    findAndValidatePersons( inputFrame, inputFrame, foregroundFrame );

    time += timer.elapsed();
    trackers += kcfTrackers.size();
    counter++;

    if ( ( counter % 25 ) == 0 ) {
        std::cout << "Operation took" << time / 25. << "milliseconds, tracking number: "
                  << trackers / 25. << std::endl;
        time = 0;
        trackers = 0;
    }
}

void TrackerManager::initNewKCFTracking( const cv::Mat &inputFrame, const cv::Rect &bounds ) {
    static qint32 trackersCounter = 0;
    cv::Ptr<KCFTracker> tracker = new KCFTracker( !RAW_PIXELS, true, true, !RAW_PIXELS );
    tracker->init( bounds, inputFrame );
    kcfTrackers.insert( trackersCounter++, qMakePair( tracker, Person( bounds ) ) );
}

void TrackerManager::resetAlgorithmState() {
    bgSubstractor = cv::createBackgroundSubtractorMOG2();
    kcfTrackers.clear();
    hog = new cv::HOGDescriptor();
    hog->setSVMDetector( cv::HOGDescriptor::getDefaultPeopleDetector() );
    init = true;
}

void TrackerManager::setROI( const QRect &roi ) {
    int x, y, width, height;
    roi.getRect( &x, &y, &width, &height );
    regionOfInterest = cv::Rect( x, y, width, height );
    openROI = regionOfInterest; //TEST
    areaThreshold = width * height / 8;
    init = true;
#ifdef DEBUG
    std::cout << "Area min threshold: " << areaThreshold << std::endl;
#endif
}

double TrackerManager::getMinimumThreshold() {
    return areaThreshold;
}

void TrackerManager::updateCurrentKCFTracking( cv::Mat &inputFrame ) {
    std::vector<int> trackersLost;

    for ( auto &trackerId : kcfTrackers.keys() ) {
        auto &tracker = kcfTrackers[trackerId].first;
        auto &person = kcfTrackers[trackerId].second;

        if ( person.shouldUseDetection() ) {
            tracker->init( person.getDetectionROI(), inputFrame );
        } else {
            if ( DEBUG_MODE ) {
                std::cout << "Peak: " << tracker->peak_value << " | ";
            }

            cv::Rect updatedPerson = tracker->update( inputFrame );

            person.setROI( updatedPerson );

            if ( tracker->peak_value < 0.08 ) {//TODO decide if necessary
                trackersLost.push_back( trackerId );
                continue;
            }
        }

        auto &updatedTrackingRect = person.getROI();
        auto updatedCenterX = updatedTrackingRect.x + updatedTrackingRect.width / 2.;
        auto updatedCenterY = updatedTrackingRect.y + updatedTrackingRect.height / 2.;

        if ( updatedCenterX > inputFrame.cols || updatedCenterX < 0
                || updatedCenterY > inputFrame.rows || updatedCenterY < 0 ) {
            trackersLost.push_back( trackerId );
        }

        person.draw( inputFrame );
    }

    if ( DEBUG_MODE ) {
        std::cout << std::endl;
    }

    for ( auto trackerId : trackersLost ) {
        if ( kcfTrackers.contains( trackerId ) ) {
            kcfTrackers.remove( trackerId );
        }
    }

}

void TrackerManager::adjustHogResponse( cv::Rect &foundRect ) {
    foundRect.x += cvRound( foundRect.width * 0.08 );
    foundRect.width = cvRound( foundRect.width * 0.9 );
    foundRect.y += cvRound( foundRect.height * 0.05 );
    foundRect.height = cvRound( foundRect.height * 0.9 );
}

void TrackerManager::adjustHogRect( cv::Mat &inputFrame, cv::Rect &rect ) {
    rect.x = std::max( rect.x - ( rect.width / 2. ), 0.0 );
    rect.y = std::max( rect.y - ( rect.height / 6. ), 0.0 );
    rect.width = std::min( rect.width * 2, inputFrame.cols - rect.x );
    rect.height = std::min( static_cast<int>( rect.height * 4. / 3. ), inputFrame.rows - rect.y );
}

void TrackerManager::findAndValidatePersons( cv::Mat &resizedFrame, cv::Mat &inputFrame, const cv::Mat &foregroundFrame ) {
    std::vector<std::vector<cv::Point>> contours;

    cv::findContours( foregroundFrame, contours, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_NONE );
    std::vector<cv::Rect> detectionBounds;

    for ( const auto &contour : contours ) {
        processBlob( inputFrame, contour );
    }

    validateTrackers( inputFrame );
    removeOverlap();
}

void TrackerManager::processBlob( cv::Mat &inputFrame, const std::vector<cv::Point> &contour ) {
    std::vector<cv::Rect> found;
    std::vector<double> foundWeights;
    double xScale, yScale;
    cv::Mat resizedPerson;

    cv::Moments moments = cv::moments( contour );
    cv::Point contourCenter( moments.m10 / moments.m00, moments.m01 / moments.m00 );

    if ( contourArea( contour ) <= 200.0 ) {//Filtering out noise
        return;
    }

    auto rect = boundingRect( contour );

    adjustHogRect( inputFrame, rect );

    xScale = rect.width / 64.;
    yScale = rect.height / 128.;

    if ( xScale < 1. || yScale < 1. ) {
        cv::resize( inputFrame( rect ), resizedPerson, cv::Size( 64, 128 ) );
    } else {
        xScale = 1.;
        yScale = 1.;
        resizedPerson = inputFrame( rect );
    }

    if ( ENABLED_HOG_RECT ) {
        cv::rectangle( inputFrame, rect.tl(), rect.br(), cv::Scalar( 0, 255, 0 ), 1 );
    }

    hog->detectMultiScale( resizedPerson, found, foundWeights, 0, cv::Size( CELL_SIZE, CELL_SIZE ), cv::Size( 32, 32 ), 1.2 );

    for ( size_t i = 0; i < found.size(); i++ ) {
        adjustHogResponse( found[i] ); // shrink HOG responses slightly

        cv::Rect foundPerson( found[i].x * xScale + rect.x, found[i].y * yScale + rect.y, found[i].width * xScale, found[i].height * yScale );

        if ( ENABLED_HOG_DETECTED_RECT ) {
            cv::rectangle( inputFrame, foundPerson, cv::Scalar( 255, 0, 0 ), 1 );
        }

        addNewPerson( inputFrame, foundPerson );
    }
}

void TrackerManager::validateTrackers( cv::Mat &inputFrame ) {
    std::vector<cv::Rect> found;
    std::vector<double> foundWeights;
    double xScale, yScale;
    cv::Mat resizedPerson;
    std::vector<qint32> trackersToDelete;

    for ( auto &key : kcfTrackers.keys() ) {
        auto &person = kcfTrackers[key].second;

        if ( !person.isDetected() ) {
            found.clear();

            auto rect = person.getROI();
            adjustHogRect( inputFrame, rect );

            xScale = rect.width / 64.;
            yScale = rect.height / 128.;

            if ( xScale < 1. || yScale < 1. ) {
                cv::resize( inputFrame( rect ), resizedPerson, cv::Size( 64, 128 ) );
            } else {
                xScale = 1.;
                yScale = 1.;
                resizedPerson = inputFrame( rect );
            }

            hog->detectMultiScale( resizedPerson, found, foundWeights, 0, cv::Size( CELL_SIZE, CELL_SIZE ), cv::Size( 32, 32 ), 1.2 );

            if ( found.size() > 0 ) {
                auto maxWeightIndex = 0;
                auto maxWeightValue = 0.;

                for ( int i = 0; i < foundWeights.size(); i++ ) {
                    if ( foundWeights[i] > maxWeightValue ) {
                        maxWeightValue = foundWeights[i];
                        maxWeightIndex = i;
                    }
                }

                auto &foundRect = found[maxWeightIndex];
                adjustHogResponse( foundRect );
                cv::Rect foundPerson( foundRect.x * xScale + rect.x, foundRect.y * yScale + rect.y, foundRect.width * xScale, foundRect.height * yScale );

                if ( ENABLED_HOG_DETECTED_RECT ) {
                    cv::rectangle( inputFrame, foundPerson, cv::Scalar( 255, 0, 0 ), 1 );
                }

                person.detection( foundPerson );
            } else {
                person.detectionMiss();

                if ( person.isLost() ) {
                    trackersToDelete.push_back( key );
                }
            }
        }

        person.resetDetection();
    }

    for ( auto trackerKey : trackersToDelete ) {
        if ( kcfTrackers.contains( trackerKey ) ) {
            kcfTrackers.remove( trackerKey );
        }
    }
}

void TrackerManager::addNewPerson( cv::Mat &inputFrame, cv::Rect &foundPerson ) {
    bool personExists {false};

    for ( auto &trackerId : kcfTrackers.keys() ) {
        auto &person = kcfTrackers[trackerId].second;

        if ( person.isInBounds( foundPerson ) ) {
            personExists = true;
            person.detection( foundPerson );
        }
    }

    if ( !personExists ) {
        if ( !reLabelTrackers( foundPerson ) ) {
            initNewKCFTracking( inputFrame, foundPerson );
        }
    }
}

void TrackerManager::removeOverlap() {
    std::vector<int> matchingTrackers;
    QVector<int> trackersToDelete;
    auto remainingKeys = kcfTrackers.keys();

    for ( auto &trackerId : kcfTrackers.keys() ) {
        remainingKeys.removeOne( trackerId );

        for ( auto &remainingId : remainingKeys ) {
            if ( kcfTrackers[trackerId].second.getDetectionROI() == kcfTrackers[remainingId].second.getDetectionROI() ) {
                matchingTrackers.push_back( trackerId );
                matchingTrackers.push_back( remainingId );
            }
        }
    }

    for ( auto trackerId : kcfTrackers.keys() ) {
        auto &person = kcfTrackers[trackerId].second;

        if ( std::find( matchingTrackers.begin(), matchingTrackers.end(), trackerId ) != matchingTrackers.end() ) {
            person.grouped();

            if ( person.isOverlapped() ) {
                trackersToDelete.push_back( trackerId );
            }
        } else {
            person.notGrouped();
        }
    }

    auto remainingTrackersToDelete = trackersToDelete;

    for ( auto trackerKey : trackersToDelete ) {
        auto leftROIs = 0;

        if ( remainingTrackersToDelete.contains( trackerKey ) ) {
            remainingTrackersToDelete.removeOne( trackerKey );
        }

        for ( auto remaining : remainingTrackersToDelete ) {
            if ( kcfTrackers[remaining].second.getDetectionROI() == kcfTrackers[trackerKey].second.getDetectionROI()
                    && trackerKey != remaining ) {
                leftROIs++;
            }
        }

        if ( leftROIs && kcfTrackers.contains( trackerKey ) ) {
            kcfTrackers.remove( trackerKey );
        }
    }

}

bool TrackerManager::reLabelTrackers( cv::Rect &foundPerson ) {
    std::vector<std::vector<int>> matchingTrackers;
    auto remainingKeys = kcfTrackers.keys();

    for ( auto &trackerId : kcfTrackers.keys() ) {
        remainingKeys.removeOne( trackerId );
        std::vector<int> trackerSameDetections;
        trackerSameDetections.push_back( trackerId );

        for ( auto &remainingId : remainingKeys ) {
            if ( kcfTrackers[trackerId].second.getDetectionROI() == kcfTrackers[remainingId].second.getDetectionROI() ) {
                trackerSameDetections.push_back( remainingId );
            }
        }

        if ( trackerSameDetections.size() > 1 ) {
            matchingTrackers.push_back( trackerSameDetections );
        }
    }

    if ( matchingTrackers.size() > 0 ) {
        auto peakValue = 1.F;
        auto trackerIdToReset = -1;

        for ( auto sameDetections : matchingTrackers ) {
            for ( auto trackerId : sameDetections ) {
                auto &person = kcfTrackers[trackerId].second;

                auto personROI = person.getROI();

                if ( person.getDistanceFromTracking( foundPerson ) <
                        sqrt( personROI.width * personROI.width + personROI.height * personROI.height ) * 2. ) {//TOOD set good threshold
                    if ( kcfTrackers[trackerId].first->peak_value < peakValue ) {
                        peakValue = kcfTrackers[trackerId].first->peak_value;
                        trackerIdToReset = trackerId;
                    }
                }
            }
        }

        if ( kcfTrackers.contains( trackerIdToReset ) ) {
            kcfTrackers[trackerIdToReset].second.detection( foundPerson );
            kcfTrackers[trackerIdToReset].second.setROI( foundPerson );

            kcfTrackers[trackerIdToReset].second.checkResetTracking();

            if ( DEBUG_MODE ) {
                std::cout << "RESET" << std::endl;
                std::cout << "RESET" << std::endl;
            }

            return true;
        } else {
            if ( DEBUG_MODE ) {
                std::cout << "ERROR COULDNT FIND TRACKER!!!" << std::endl;
            }
        }
    }

    return false;
}

void TrackerManager::addNewKCFPerson( cv::Mat &inputFrame, cv::Rect &foundPerson ) {
    bool personExists = false;

    for ( auto &trackerId : kcfTrackers.keys() ) {
        auto &person = kcfTrackers[trackerId].second;

        if ( person.isInBounds( foundPerson ) ) {
            personExists = true;
            break;
        }
    }

    if ( !personExists ) {
        initNewKCFTracking( inputFrame, foundPerson );
    }
}
