#-------------------------------------------------
#
# Project created by QtCreator 2017-10-15T19:02:58
#
#-------------------------------------------------

QT       += core gui
CONFIG += -std=c++14
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = pedestrianGUI
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

LIBS += -lopencv_highgui -lopencv_videoio -lopencv_imgcodecs -lopencv_video -lopencv_imgproc \
        -lopencv_core -lopencv_tracking -lopencv_objdetect

SOURCES += \
        main.cpp \
    KCF/fhog.cpp \
    KCF/kcftracker.cpp \
    Player/ImageLabel.cpp \
    Player/VideoPlayer.cpp \
    Player/VideoPlayerView.cpp \
    Tracking/TrackerManager.cpp \
    Tracking/Person.cpp

HEADERS += \
    KCF/ffttools.hpp \
    KCF/fhog.hpp \
    KCF/kcftracker.hpp \
    KCF/labdata.hpp \
    KCF/recttools.hpp \
    Player/ImageLabel.h \
    Player/VideoPlayer.h \
    Player/VideoPlayerView.h \
    Tracking/TrackerManager.h \
    Tracking/Person.h

FORMS +=

DISTFILES += \
    config.astylerc
