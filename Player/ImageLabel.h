#ifndef IMAGELABEL_H
#define IMAGELABEL_H
#include <QLabel>
#include <QRubberBand>
#include <QPoint>
#include <QMouseEvent>
#include <QRect>

class ImageLabel : public QLabel {
    Q_OBJECT
  public:
    ImageLabel()
        : selectionRectangle( QRubberBand::Rectangle, this ) {}

    void setROI() {
        settingROI = true;
    }
    void endSettingROI() {
        settingROI = false;
    }

    void mousePressEvent( QMouseEvent *event ) override;
    void mouseMoveEvent( QMouseEvent *event ) override;
    void mouseReleaseEvent( QMouseEvent *event ) override;

  signals:
    void selectedROI( const QRect &selection );

  private:
    bool settingROI;
    QRubberBand selectionRectangle;
    QPoint selectionOrigin;
};
#endif // IMAGELABEL_H
