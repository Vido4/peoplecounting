#include <QtWidgets>
#include "VideoPlayerView.h"

VideoPlayerView::VideoPlayerView()
    : imageLabel( new ImageLabel ) {
    imageLabel->setBackgroundRole( QPalette::Base );
    imageLabel->setScaledContents( false );
    setCentralWidget( imageLabel );

    statusBar()->setSizeGripEnabled( false );

    createActions();
    updateEnabledActions( {ACTIONS::OPEN_CAMERA, ACTIONS::OPEN_IMAGES, ACTIONS::OPEN_VIDEO} );
    player = new VideoPlayer();
    QObject::connect( player, SIGNAL( processedImage( QImage ) ), this, SLOT( updateImage( QImage ) ) );
    QObject::connect( player, SIGNAL( finishedVideo() ), this, SLOT( endOfVideo() ) );
    QObject::connect( imageLabel, SIGNAL( selectedROI( const QRect & ) ), this, SLOT( onSelectedROI( const QRect & ) ) );

    resize( QGuiApplication::primaryScreen()->availableSize() * 1 / 5 );
}

void VideoPlayerView::updateImage( const QImage &newImage ) {
    image = newImage;
    imageLabel->setPixmap( QPixmap::fromImage( image ) );
}

bool VideoPlayerView::saveFile( const QString &fileName ) {
    QImageWriter writer( fileName );

    if ( !writer.write( image ) ) {
        QMessageBox::information( this, QGuiApplication::applicationDisplayName(),
                                  tr( "Cannot write %1: %2" )
                                  .arg( QDir::toNativeSeparators( fileName ) ), writer.errorString() );
        return false;
    }

    const QString message = tr( "Wrote \"%1\"" ).arg( QDir::toNativeSeparators( fileName ) );
    statusBar()->showMessage( message );
    return true;
}

void VideoPlayerView::open() {
    QString filename = QFileDialog::getOpenFileName( this,
                       tr( "Open Video" ), ".",
                       tr( "Video Files (*.avi *.mpg *.mp4)" ),
                       0,
                       QFileDialog::DontUseNativeDialog );

    if ( !filename.isEmpty() ) {
        if ( !player->loadVideo( filename ) ) {
            QMessageBox msgBox;
            msgBox.setText( "The selected video could not be opened!" );
            msgBox.exec();
        }
    }

    updateEnabledActions( {ACTIONS::ALL} );
    const QString message = tr( "Opened \"%1\"" ).arg( QDir::toNativeSeparators( filename ) );
    statusBar()->showMessage( message );
}

void VideoPlayerView::about() {
    //    QMessageBox::about(this, tr("About Image Viewer"),
    //                       tr("<p>The <b>Image Viewer</b> example shows how to combine QLabel "
    //                          "and QScrollArea to display an image. QLabel is typically used "
    //                          "for displaying a text, but it can also display an image. "
    //                          "QScrollArea provides a scrolling view around another widget. "
    //                          "If the child widget exceeds the size of the frame, QScrollArea "
    //                          "automatically provides scroll bars. </p><p>The example "
    //                          "demonstrates how QLabel's ability to scale its contents "
    //                          "(QLabel::scaledContents), and QScrollArea's ability to "
    //                          "automatically resize its contents "
    //                          "(QScrollArea::widgetResizable), can be used to implement "
    //                          "zooming and scaling features. </p><p>In addition the example "
    //                          "shows how to use QPainter to print an image.</p>"));
}

void VideoPlayerView::play() {
    updateEnabledActions( {ACTIONS::PAUSE} );
    player->play();
}

void VideoPlayerView::pause() {
    updateEnabledActions( {ACTIONS::ALL} );
    player->pause();
}

void VideoPlayerView::reset() {
    player->reset();
}

void VideoPlayerView::openImages() {
    QString filename = QFileDialog::getExistingDirectory( this,
                       tr( "Open Directory" ), ".",
                       QFileDialog::DontUseNativeDialog | QFileDialog::ShowDirsOnly );

    if ( !filename.isEmpty() ) {
        if ( !player->loadDirectory( filename ) ) {
            QMessageBox msgBox;
            msgBox.setText( "The selected video could not be opened!" );
            msgBox.exec();
        }
    }

    updateEnabledActions( {ACTIONS::ALL} );
    const QString message = tr( "Opened directory\"%1\"" ).arg( QDir::toNativeSeparators( filename ) );
    statusBar()->showMessage( message );
}

void VideoPlayerView::selectROI() {
    imageLabel->setROI();
    showStatusBarMessage( "[ESC to exit selection] Select person to track" );
}

void VideoPlayerView::endOfSelection() {
    imageLabel->endSettingROI();
    showStatusBarMessage( "End of selection" );
}

void VideoPlayerView::onSelectedROI( const QRect &selection ) {
    player->setROI( selection );
    endOfSelection();
}

void VideoPlayerView::showStatusBarMessage( const char *message ) {
    statusBar()->showMessage( tr( message ) );
}

void VideoPlayerView::openCamera() {

}

void VideoPlayerView::endOfVideo() {
    updateEnabledActions( {ACTIONS::ALL} );
    statusBar()->showMessage( "End of video" );
}

void VideoPlayerView::updateEnabledActions( const std::initializer_list<ACTIONS> &actionsToEnable ) {
    for ( ACTIONS action : actions.keys() ) {
        bool shouldEnableAction = false;

        for ( auto actionToEnable : actionsToEnable ) {
            if ( action == actionToEnable || actionToEnable == ACTIONS::ALL ) {
                shouldEnableAction = true;
            }
        }

        actions[action]->setEnabled( shouldEnableAction );
    }
}

void VideoPlayerView::createActions() {
    QMenu *fileMenu = menuBar()->addMenu( tr( "File" ) );
    QAction *action;

    action = fileMenu->addAction( tr( "&Open video" ), this, &VideoPlayerView::open );
    action->setShortcut( QKeySequence::Open );
    actions.insert( ACTIONS::OPEN_VIDEO, action );


    action = fileMenu->addAction( tr( "Open &images" ), this, &VideoPlayerView::openImages ) ;
    action->setShortcut( tr( "Ctrl+I" ) );
    actions.insert( ACTIONS::OPEN_IMAGES, action );

    fileMenu->addSeparator();

    action = fileMenu->addAction( tr( "Exit" ), this, &QWidget::close );
    action->setShortcut( tr( "Ctrl+Q" ) );

    QMenu *videoMenu = menuBar()->addMenu( tr( "Video" ) );

    action = videoMenu->addAction( tr( "&Play" ), this, &VideoPlayerView::play );
    action->setShortcut( tr( "Ctrl+P" ) );
    actions.insert( ACTIONS::PLAY, action );

    action = videoMenu->addAction( tr( "Pau&se" ), this, &VideoPlayerView::pause );
    action->setShortcut( tr( "Ctrl+S" ) );
    actions.insert( ACTIONS::PAUSE, action );

    action = videoMenu->addAction( tr( "&Reset" ), this, &VideoPlayerView::reset );
    action->setShortcut( tr( "Ctrl+R" ) );
    actions.insert( ACTIONS::RESET, action );

    QMenu *selectionMenu = menuBar()->addMenu( tr( "Selection" ) );

    action = selectionMenu->addAction( tr( "Select ROI" ), this, &VideoPlayerView::selectROI );
    actions.insert( ACTIONS::SELECT_ROI, action );

    action = selectionMenu->addAction( tr( "End selection" ), this, &VideoPlayerView::endOfSelection );
    action->setShortcut( tr( "Esc" ) );
    actions.insert( ACTIONS::END_SELECTION, action );

    QMenu *helpMenu = menuBar()->addMenu( tr( "Help" ) );

    helpMenu->addAction( tr( "About" ), this, &VideoPlayerView::about );
}
