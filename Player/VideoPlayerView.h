#ifndef VIDEOPLAYER_H
#define VIDEOPLAYER_H

#include <QMainWindow>
#include <QImage>
#include <QRubberBand>
#include <QHash>
#include <QAction>
#include <QElapsedTimer>
#include "ImageLabel.h"
#include "VideoPlayer.h"

class QAction;
class QLabel;
class QMenu;
class QScrollArea;
class QScrollBar;

class VideoPlayerView : public QMainWindow {
    Q_OBJECT

  public:
    VideoPlayerView();

  private slots:
    void open();
    void play();
    void pause();
    void reset();
    void openCamera();
    void selectROI();
    void openImages();
    void about();
    void updateImage( const QImage &frame );
    void endOfVideo();
    void endOfSelection();
    void onSelectedROI( const QRect &selection );
  private:

    enum ACTIONS {
        OPEN_CAMERA,
        OPEN_VIDEO,
        OPEN_IMAGES,
        PLAY,
        PAUSE,
        RESET,
        SELECT_ROI,
        END_SELECTION,
        ALL
    };
    void showStatusBarMessage( const char *message );
    void updateEnabledActions( const std::initializer_list<ACTIONS> &actionsToEnable );
    void createActions();
    bool saveFile( const QString &fileName );

    bool settingROI = false;
    QImage image;
    ImageLabel *imageLabel;
    QHash<ACTIONS, QAction *> actions;
    VideoPlayer *player;
};

#endif
