#include "VideoPlayer.h"
#include <iostream>
VideoPlayer::VideoPlayer( QObject *parent )
    : QThread( parent ) {
    stop = true;
}

bool VideoPlayer::loadVideo( QString filename ) {
    Mat openingFrame;
    String video( filename.toStdString() );
    capture.open( video );

    if ( capture.isOpened() ) {
        frameRate = ( int ) capture.get( CV_CAP_PROP_FPS );
        timePerFrame = 1000. / frameRate;

        if ( capture.read( openingFrame ) ) {
            capture.set( CAP_PROP_POS_FRAMES, 0 );
        }

        fileInfoList.clear();
        tracker.resetAlgorithmState();
        processAndEmitImage( openingFrame );
        return true;
    } else
        return false;
}

bool VideoPlayer::loadDirectory( QString dirname ) {
    Mat openingFrame;
    QDir directory( dirname );

    currentDirname = dirname;
    directory.setFilter( QDir::Files );
    fileInfoList = directory.entryInfoList();

    currentFrame = startingFrame;
    frameRate = 25;
    timePerFrame = 1000. / frameRate;

    openingFrame = imread( getFrameName( currentFrame ) );
    tracker.resetAlgorithmState();
    processAndEmitImage( openingFrame );

    return true;
}

String VideoPlayer::getFrameName( int frameNumber ) {
    return String( ( currentDirname + "/" + fileInfoList.at( frameNumber ).fileName() ).toStdString() );
}

QImage VideoPlayer::frameToImage( const Mat &frame ) {
    QImage::Format frameFormat;

    if ( frame.channels() == 3 ) {
        cv::cvtColor( frame, frame, CV_BGR2RGB );
        frameFormat = QImage::Format_RGB888;
    } else {
        frameFormat = QImage::Format_Indexed8;
    }

    return QImage( ( const unsigned char * )( frame.data ), frame.cols, frame.rows, frameFormat );
}

void VideoPlayer::setROI( const QRect &selection ) {
    tracker.setROI( selection );
}

double VideoPlayer::getMinimumThreshold() {
    return tracker.getMinimumThreshold();
}

void VideoPlayer::play() {
    if ( !isRunning() ) {
        stop = false;
        start( LowPriority );
    }
}

void VideoPlayer::run() {
    if ( fileInfoList.size() > 0 ) {
        imagesRun();
    } else {
        videoRun();
    }
}

void VideoPlayer::videoRun() {
    Mat frame;

    while ( !stop ) {
        if ( !capture.read( frame ) ) {
            stop = true;
            emit finishedVideo();
            break;
        }

        rateTimer.start();

        processAndEmitImage( frame );

        auto elapsed = rateTimer.elapsed();

        if ( elapsed <  timePerFrame ) {
            QThread::currentThread()->usleep( ( timePerFrame - elapsed ) * 1000 );
        }
    }

    QThread::currentThread()->usleep( 10000 );

}

void VideoPlayer::imagesRun() {
    Mat frame;

    while ( !stop ) {
        if ( currentFrame >= fileInfoList.size() ) {
            stop = true;
            emit finishedVideo();
            break;
        }

        rateTimer.start();

        frame = imread( getFrameName( currentFrame ) );
        processAndEmitImage( frame );

        auto elapsed = rateTimer.elapsed();

        if ( elapsed < timePerFrame ) {//TODO slow down if too fast processing
            QThread::currentThread()->usleep( ( timePerFrame - elapsed ) * 1000 );
        }

        currentFrame++;
    }

    QThread::currentThread()->usleep( 10000 );
}

void VideoPlayer::processAndEmitImage( cv::Mat &inputFrame ) {
    tracker.performTracking( inputFrame, currentFrame );
    emit processedImage( frameToImage( inputFrame ) );
}

VideoPlayer::~VideoPlayer() {
    mutex.lock();
    stop = true;
    capture.release();
    condition.wakeOne();
    mutex.unlock();
    wait();
}

void VideoPlayer::pause() {
    stop = true;
}

void VideoPlayer::reset() {
    Mat openingFrame;

    if ( stop ) {
        if ( fileInfoList.size() > 0 ) {
            openingFrame = resetImages();
        } else {
            if ( !resetVideo( openingFrame ) ) {
                return;
            }
        }

        tracker.resetAlgorithmState();
        processAndEmitImage( openingFrame );
    }
}

Mat VideoPlayer::resetImages() {
    currentFrame = startingFrame;
    return imread( getFrameName( currentFrame ) );
}

bool VideoPlayer::resetVideo( Mat &openingFrame ) {
    if ( capture.isOpened() ) {
        capture.set( CAP_PROP_POS_FRAMES, 0 );

        if ( capture.read( openingFrame ) ) {
            capture.set( CAP_PROP_POS_FRAMES, 0 );
        }

        return true;
    }

    return false;
}

void VideoPlayer::msleep( int ms ) {
    struct timespec ts = { ms / 1000, ( ms % 1000 ) * 1000 * 1000 };
    nanosleep( &ts, NULL );
}
