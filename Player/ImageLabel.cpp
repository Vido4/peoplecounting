#include "ImageLabel.h"
#include <iostream>
void ImageLabel::mousePressEvent( QMouseEvent *event ) {
    if ( settingROI ) {
        selectionOrigin = event->pos();
        selectionRectangle.setGeometry( QRect( selectionOrigin, QSize() ) );
        selectionRectangle.show();
    }
}

void ImageLabel::mouseMoveEvent( QMouseEvent *event ) {
    if ( settingROI ) {
        selectionRectangle.setGeometry( QRect( selectionOrigin, event->pos() ).normalized() );
    }
}

void ImageLabel::mouseReleaseEvent( QMouseEvent *event ) {
    if ( settingROI ) {
        selectionRectangle.hide();
        auto x = selectionRectangle.geometry();
        std::cout << "Width: " << x.width() << ", Height: " << x.height();
        emit selectedROI( selectionRectangle.geometry() );
    }
}
