#ifndef PLAYER_H
#define PLAYER_H
#include <QDir>
#include <QMutex>
#include <QThread>
#include <QImage>
#include <QWaitCondition>
#include <QFileInfoList>
#include <QElapsedTimer>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "../Tracking/TrackerManager.h"

using namespace cv;
class VideoPlayer : public QThread {
    Q_OBJECT
  private:
    bool stop;
    QMutex mutex;
    QWaitCondition condition;
    int frameRate;
    int timePerFrame;
    int currentFrame;
    VideoCapture capture;
    QFileInfoList fileInfoList;
    QString currentDirname;
    TrackerManager tracker;
    QElapsedTimer rateTimer;
    static constexpr int startingFrame = 0;

  signals:
    void processedImage( const QImage &image );
    void finishedVideo();
  protected:
    void run();
    void msleep( int ms );

  private:
    void videoRun();
    void imagesRun();
    Mat resetImages();
    bool resetVideo( cv::Mat &inputFrame );
    QImage frameToImage( const Mat &openingFrame );
    String getFrameName( int frameNumber );
    void processAndEmitImage( cv::Mat &inputFrame );

  public:
    VideoPlayer( QObject *parent = 0 );
    ~VideoPlayer();

    void setROI( const QRect &minumumThreshold );
    void setMaximumThreshold( const QRect &maximumThreshold );
    double getMinimumThreshold();
    std::pair<double, double> getMaximumThreshold();

    bool loadVideo( QString filename );
    bool loadDirectory( QString dirname );
    void play();
    void pause();
    void reset();
};
#endif // VIDEOPLAYER_H
